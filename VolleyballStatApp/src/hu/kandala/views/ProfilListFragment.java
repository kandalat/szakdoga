package hu.kandala.views;

import hu.kandala.adapters.ProfilesListAdapter;
import hu.kandala.entities.User;
import hu.kandala.functions.UserFunctions;
import hu.kandala.volleyballstatapp.R;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class ProfilListFragment extends Fragment
	{
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState)
			{
				// TODO Auto-generated method stub
				View rootView = inflater.inflate(R.layout.profiles, container,
						false);
				UserFunctions uf = new UserFunctions();
				ArrayList<User> list = uf.getUserList();
				for (User u : list){
				Log.i("Tamás",u.getName());
				}
				
				if(list.isEmpty())
				{
					AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
					
					builder.setMessage("Hozz létre egy profilt!");
					builder.setTitle("Nincs elérhető profil");
					builder.setPositiveButton("Profil Létrehozása", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					});
					builder.create();
					builder.show();
				}
				
				ListView listView = (ListView) rootView
						.findViewById(R.id.profilesListID);

				ProfilesListAdapter adapter = new ProfilesListAdapter(
						getActivity().getApplicationContext(),
						R.layout.profiles_list_item, uf.getUserList());

				listView.setAdapter(adapter);

				return rootView;
			}
	}
