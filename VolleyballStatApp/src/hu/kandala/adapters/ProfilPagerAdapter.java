package hu.kandala.adapters;

import hu.kandala.views.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ProfilPagerAdapter extends FragmentPagerAdapter
{
	public ProfilPagerAdapter(FragmentManager fm)
	{
		super(fm);
	}

	@Override
	public Fragment getItem(int item)
	{
		switch (item)
		{
		case 0:
			ProfilListFragment profilListFragment = new ProfilListFragment();
			return profilListFragment;
		case 1:
			ProfilDataFragment profilDataFragment = new ProfilDataFragment();
			return profilDataFragment;
		case 2:
			
	
		default:
			return new ProfilDataFragment();
		}
	}

	@Override
	public int getCount()
	{
		return 2;
	}
}