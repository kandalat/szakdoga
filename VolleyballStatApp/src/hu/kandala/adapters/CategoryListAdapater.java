package hu.kandala.adapters;

import hu.kandala.entities.Category;
import hu.kandala.volleyballstatapp.CategoryInputButtonOnClickListener;
import hu.kandala.volleyballstatapp.R;
import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CategoryListAdapater extends ArrayAdapter<Category>
	{

		public CategoryListAdapater(Context context, int resource,
				List<Category> objects)
			{
				super(context, resource, objects);
				// TODO Auto-generated constructor stub
			}

		@Override
		public View getView(int position, View convertView, ViewGroup parent)
			{

				Category item = getItem(position);

				View v = convertView;
				LayoutInflater vi;
				vi = LayoutInflater.from(getContext());
				v = vi.inflate(R.layout.categories_list_item, null);
				// Elemek deklarálása és értékadás

				// Kategória neve + szummaérték
				TextView categoryNameTextView = (TextView) v
						.findViewById(R.id.categoryNameTextViewID);

				categoryNameTextView.setText(item.getName());

				EditText categorySumValueEditText = (EditText) v
						.findViewById(R.id.categorySumValueEditTextID);

				categorySumValueEditText.setText(item.getValue().toString());

				// Jóérték neve + érték + gombok
				TextView goodValueNameTextView = (TextView) v
						.findViewById(R.id.goodValueNameTextViewID);

				goodValueNameTextView.setText(item.getGoodValueName());

				EditText goodValueEditText = (EditText) v
						.findViewById(R.id.goodValueEditTextID);

				goodValueEditText.setText(item.getGoodValue().toString());

				Button goodPlusButton = (Button) v
						.findViewById(R.id.goodValuePlusButtonID);
				goodPlusButton.setTag("goodValuePlusButton");
				goodPlusButton
						.setOnClickListener(new CategoryInputButtonOnClickListener(
								item, categorySumValueEditText,
								goodValueEditText));

				Button goodMinusButton = (Button) v
						.findViewById(R.id.goodValueMinusButtonID);
				goodMinusButton.setTag("goodValueMinusButton");
				goodMinusButton
						.setOnClickListener(new CategoryInputButtonOnClickListener(
								item, categorySumValueEditText,
								goodValueEditText));

				// Közepesérték neve + érték + gombok
				TextView midValueNameTextView = (TextView) v
						.findViewById(R.id.midValueNameTextViewID);

				midValueNameTextView.setText(item.getNotBadValueName());

				EditText midValueEditText = (EditText) v
						.findViewById(R.id.midValueEditTextID);

				midValueEditText.setText(item.getNotBadValue().toString());

				Button midPlusButton = (Button) v
						.findViewById(R.id.midValuePlusButtonID);
				midPlusButton.setTag("midValuePlusButton");
				midPlusButton
						.setOnClickListener(new CategoryInputButtonOnClickListener(
								item, categorySumValueEditText,
								midValueEditText));

				Button midMinusButton = (Button) v
						.findViewById(R.id.midValueMinusButtonID);
				midMinusButton.setTag("midValueMinusButton");
				midMinusButton
						.setOnClickListener(new CategoryInputButtonOnClickListener(
								item, categorySumValueEditText,
								midValueEditText));

				// Rosszérték neve + érték + gombok
				TextView badValueNameTextView = (TextView) v
						.findViewById(R.id.badValueNameTextViewID);

				badValueNameTextView.setText(item.getBadValueName());

				EditText badValueEditText = (EditText) v
						.findViewById(R.id.badValueEditTextID);

				badValueEditText.setText(item.getBadValue().toString());

				Button badPlusButton = (Button) v
						.findViewById(R.id.badValuePlusButtonID);
				badPlusButton.setTag("badValuePlusButton");
				badPlusButton
						.setOnClickListener(new CategoryInputButtonOnClickListener(
								item, categorySumValueEditText,
								badValueEditText));

				Button badMinusButton = (Button) v
						.findViewById(R.id.badValueMinusButtonID);
				badMinusButton.setTag("badValueMinusButton");
				badMinusButton
						.setOnClickListener(new CategoryInputButtonOnClickListener(
								item, categorySumValueEditText,
								badValueEditText));

				// TODO Auto-generated method stub
				return v;
			}

	}
