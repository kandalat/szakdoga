package hu.kandala.adapters;

import hu.kandala.entities.User;
import hu.kandala.volleyballstatapp.R;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ProfilesListAdapter extends ArrayAdapter<User> implements OnItemClickListener
	{

	public ProfilesListAdapter(Context context, int resource,
			 List<User> objects)
		{
			super(context, resource, objects);
			// TODO Auto-generated constructor stub
		}
	public View getView(int position, View convertView, ViewGroup parent)
		{
			User item = getItem(position);

			View view = convertView;
			
			LayoutInflater layoutInflater = LayoutInflater.from(getContext());
			
			view = layoutInflater.inflate(R.layout.profiles_list_item, null);
			
			TextView userNameTextView = (TextView) view.findViewById(R.id.userNameTextViewID);
			userNameTextView.setTextColor(Color.WHITE);
			
			
		
			userNameTextView.setText(item.getUserName());		
			System.out.println(userNameTextView.getText());
			return view;
		}
	@Override
	public void onItemClick(AdapterView<?> parent, View v, int positon, long id) {
		// TODO Auto-generated method stub
		Log.i("teg",this.getItem(positon).getName());
	}
	}
