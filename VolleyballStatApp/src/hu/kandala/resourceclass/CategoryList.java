package hu.kandala.resourceclass;

import hu.kandala.entities.Category;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kandala Tamás
 */

/**
 * 
 * @author T netbook
 * 
 *         Az osztály a Category osztály listaként történő kezelését valósítja
 *         meg. Tartalmazza a fájl olvasást és fájlba írást valamint a szöveges
 *         kiértékelés megvalósítását.
 * 
 */
public class CategoryList
	{
		/**
		 * CategoryList osztály változó: list egy lista amely Category-k ból
		 * áll. texts String típusú tömb amely a randomTextek beállításához
		 * szükséges
		 */

		private ArrayList<Category> list;
		String[] texts = { "A vizsgált szempontok alapján",
				"A bevitt adatok szerint", "A mutatott játék szerint",
				"A statisztikai számítások eredményei szerint",
				"Az aktuális szettben nyújtott teljesítmény alapján",
				"A játék képének alakulásának következményében",
				"z eddig mutatott teljesítmény alapján"

		};

		/**
		 * Üres konstruktor a CategoryListhez
		 */

		public CategoryList()
			{

			}

		/**
		 * Fájl olvasáshoz használt konstruktor
		 * 
		 * fájlba minden sor egy category-t tartalmaz amely adatok ","
		 * karakterrel vannak elválasztva
		 * 
		 * @param file
		 *            megadja az olvasni kívánt fájl abszolút elérési útját
		 * @throws FileNotFoundException
		 *             Fájl nem található kivétel
		 */

		public CategoryList(String file) throws FileNotFoundException
			{
				FileReader fr = new FileReader(file);
				BufferedReader br = new BufferedReader(fr);
				ArrayList<Category> l = new ArrayList<Category>();
				Scanner sc = new Scanner(br);

				while (sc.hasNext()) {

					sc.useDelimiter(",");
					String Name = sc.next();
					String GoodValueName = sc.next();
					String NotBadValueName = sc.next();
					String BadValueName = sc.next();
					int GoodValue = sc.nextInt();
					int NotBadValue = sc.nextInt();
					int BadValue = sc.nextInt();
					l.add(new Category(Name, GoodValueName, NotBadValueName,
							BadValueName, GoodValue, NotBadValue, BadValue));

				}

				this.list = l;
				sc.close();
			}

		/**
		 * Konstruktor amely vár egy ArrayList változót
		 * 
		 * @param list
		 *            Beállítja az objektum list változóját
		 */

		public CategoryList(ArrayList<Category> list)
			{
				this.list = list;
			}

		/**
		 * 
		 * @return Visszaadja a objektum listáját
		 */

		public ArrayList<Category> getList()
			{
				return list;
			}

		/**
		 * 
		 * @param list
		 *            beállítja az objektum lista változóját
		 */

		public void setList(ArrayList<Category> list)
			{
				this.list = list;
			}

		/**
		 * Bejárja majd kiírja az objektum lista változójának kiértékelését
		 */

		public void print()
			{
				for (Category j : this.getList()) {
					j.print();
					System.out.println();

				}
				System.out.println("");
			}

		/**
		 * Az objektum lista változójának tartalmát beírja egy fájlba A fájlban
		 * egy sor egy Category típusú változónak felel meg, A Category változói
		 * "," karakterrel vannak elválasztva
		 * 
		 * @param file
		 *            A fájl elérési útját tartalmazó string
		 */
		public void listToFile(String file)
			{
				try {
					PrintWriter pw = new PrintWriter(file);
					for (Category c : this.list) {

						pw.write(c.getName());
						pw.write(",");
						pw.write(c.getGoodValueName());
						pw.write(",");
						pw.write(c.getNotBadValueName());
						pw.write(",");
						pw.write(c.getBadValueName());
						pw.write(",");
						pw.write(c.getGoodValue().toString());
						pw.write(",");
						pw.write(c.getNotBadValue().toString());
						pw.write(",");
						pw.write(c.getBadValue().toString());
						pw.write(",");

					}
					pw.close();
				}
				catch (FileNotFoundException ex) {
					Logger.getLogger(CategoryList.class.getName()).log(
							Level.SEVERE, null, ex);
				}

			}

		/**
		 * A változatos megfogalmazás érdekében a szöveges kiértékeléshez
		 * véletlenszerű kötő mondatokat ad az objektum listájában lévő catagory
		 * objektumok randomText változójához
		 * 
		 * @return Visszaad egy CategoryList objektumot amely már tartalmazza a
		 *         szöveges kiértékeléshez szükséges véletlenszerűen generált
		 *         töltelék mondatokkal
		 */
		public CategoryList randomizeEvalutate()
			{

				CategoryList result = new CategoryList(this.getList());
				Random rd = new Random();
				int lastindex = 0;
				int index = 0;

				for (Category r : result.getList()) {
					while (lastindex == index) {
						index = rd.nextInt(texts.length);

					}

					r.setRandomText(texts[index]);

					lastindex = index;

				}
				return result;
			}

	}
