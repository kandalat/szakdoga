package hu.kandala.volleyballstatapp;

import hu.kandala.entities.Category;
import hu.kandala.functions.CategoryFunctions;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class CategoryInputButtonOnClickListener implements OnClickListener
	{
		public Category category;
		public EditText sumValue;
		public EditText subValue;

		public CategoryInputButtonOnClickListener(Category category,
				EditText sumValue, EditText subValue)
			{
				super();
				this.category = category;
				this.sumValue = sumValue;
				this.subValue = subValue;
			}

		@Override
		public void onClick(View v)
			{
				// TODO Auto-generated method stub
				Button button = (Button)v; 
				CategoryFunctions catFunc = new CategoryFunctions();
				if (button.getText().equals("+"))
				{
					 if (button.getTag().equals("badValuePlusButton"))
					   {
					  subValue.setText(catFunc.plusBadValue(category).toString());
				            sumValue.setText(category.getValue().toString());
					   }
					   else if (button.getTag().equals("goodValuePlusButton"))
					   {
					 subValue.setText(catFunc.plusGoodValue(category).toString());
				            sumValue.setText(category.getValue().toString());
					   }
				           
				           else if (button.getTag().equals("midValuePlusButton"))
					   {
					  subValue.setText(catFunc.plusNotBadValue(category).toString());
				            sumValue.setText(category.getValue().toString());
					   }
				}
				           else if (button.getText().toString().equals("-"))
					  {
					   if (button.getTag().equals("badValueMinusButton"))
					   {
					   subValue.setText(catFunc.minusBadValue(category).toString());
				            sumValue.setText(category.getValue().toString());
					   } 
					   else if (button.getTag().equals("goodValueMinusButton"))
					   {
					  subValue.setText(catFunc.minusGoodValue(category).toString());
				           sumValue.setText(category.getValue().toString());
					   }
				           
				            else if (button.getTag().equals("midValueMinusButton"))
					   {
					   subValue.setText(catFunc.minusNotBadValue(category).toString());
				            sumValue.setText(category.getValue().toString());
					   }

				}
				
			
			}

	}
