package hu.kandala.volleyballstatapp;

import hu.kandala.adapters.ProfilPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

public class ProfilPagerActivity extends ActionBarActivity implements
		TabListener, OnPageChangeListener
	{
		private ViewPager swipeView;

		@Override
		protected void onCreate(Bundle savedInstanceState)
			{
				super.onCreate(savedInstanceState);
				setContentView(R.layout.pager_layout);
				ProfilPagerAdapter pagerProfil = new ProfilPagerAdapter(
						getSupportFragmentManager());
				swipeView = (ViewPager) findViewById(R.id.pager);
				swipeView.setAdapter(pagerProfil);
				swipeView.setOnPageChangeListener(this);

				ActionBar actionBar = getSupportActionBar();
				actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

			
				
				actionBar.addTab(actionBar.newTab().setText("Statisztikák")
						.setTabListener(this));
				actionBar.addTab(actionBar.newTab().setText("Profil Adatok")
						.setTabListener(this));
			}

		@Override
		public void onTabReselected(Tab arg0,
				android.support.v4.app.FragmentTransaction arg1)
			{
				// TODO Auto-generated method stub

			}

		@Override
		public void onTabSelected(Tab tab,
				android.support.v4.app.FragmentTransaction arg1)
			{
				swipeView.setCurrentItem(tab.getPosition(), true);

			}

		@Override
		public void onTabUnselected(Tab arg0,
				android.support.v4.app.FragmentTransaction arg1)
			{
				// TODO Auto-generated method stub

			}

		@Override
		public void onPageScrollStateChanged(int arg0)
			{
				// TODO Auto-generated method stub

			}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2)
			{
				// TODO Auto-generated method stub

			}

		@Override
		public void onPageSelected(int position)
			{
				Log.d("TAG", "Page selected: " + position);
				getSupportActionBar().setSelectedNavigationItem(position);

			}
	}
