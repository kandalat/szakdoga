package hu.kandala.volleyballstatapp;

 

import java.util.Arrays;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity implements OnItemClickListener
{
	private static final String TAG = "Filter";
	private DrawerLayout drawerLayout;
	private ListView drawerMenu;

	private ActionBarDrawerToggle drawerToggle;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		this.getSupportActionBar().setDisplayShowTitleEnabled(false);
		setContentView(R.layout.activity_main);
		
		

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout); // ilyen drawerLayoutnak kell lennie a gyökér
		drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START); // ha nincs árnyák, nem nagy gond

		getSupportActionBar().setDisplayHomeAsUpEnabled(true); // ez mindkettő kell,
		getSupportActionBar().setHomeButtonEnabled(true); // hogy az actionbaron az appikont be lehessen nyomni
		
		List<String> navItems = Arrays.asList( "Profil", "Kategóriák", "Értékelés", "Diagrammok", "Súgó");
		drawerMenu = (ListView) findViewById(R.id.left_drawer); // sima listview lesz a drawer menu
		drawerMenu.setOnItemClickListener(this);
		drawerMenu.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
				navItems));

		// ez valósítja meg a navigation drawer ki-be csúsztatását az appikonnal
		drawerToggle = new ActionBarDrawerToggle(
				this,
				drawerLayout,
				R.drawable.ic_drawer,
				R.string.open_drawer, //teljesen mindegy milyen string
				R.string.close_drawer);
				
		drawerLayout.setDrawerListener(drawerToggle);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		drawerToggle.syncState(); // Ez kell, hogy az ikon ki-be csússzon, de nem kötelező sztem.
		super.onPostCreate(savedInstanceState);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Ha az ActionBarActivity van kiterjesztve, ebből automatán ActionBar lesz :)
		// Az activity témája android:theme="@style/Theme.AppCompat.Light" vagy valami hasonló legyen
		getMenuInflater().inflate(R.menu.edit_actionbar, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Ezzel kerül az action bar-on a klikk esemény átadásra a drawerToggle-nek.
		// Ha igazzal tér vissza, akkor az app ikonja lett megynova.
		if (drawerToggle.onOptionsItemSelected(item))
			return true;

		// Továbiakban semmi extra.
	
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		// Kis extra, hogy a hardveres menügombbal (ha van) is előjöjjön a drawer
		// Csak arra kell figyelni, hogy ne legyen olyan menüitem, ami nem fér ki az avtionbarra.
		if (event.getKeyCode() == KeyEvent.KEYCODE_MENU)
		{
			if (drawerLayout.isDrawerOpen(drawerMenu))
				drawerLayout.closeDrawer(drawerMenu);
			else
				drawerLayout.openDrawer(drawerMenu);
		}
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
	{
		// a menü listview listenerje
		Log.d(TAG, "Megynomott gomb: " + position);
		drawerLayout.closeDrawer(drawerMenu);
		
		switch (position)
		
			{
			case 0:
				
				startActivity(new Intent(getApplicationContext(),ProfilPagerActivity.class));
				break;
			case 1:
				startActivity(new Intent(getApplicationContext(),CategoriesActivity.class));
				break;
			case 2:
				startActivity(new Intent(getApplicationContext(),EvaluationActivity.class));
				break;
			case 3:
				startActivity(new Intent(getApplicationContext(),DiagramActivity.class));
				break;
			case 4:
				startActivity(new Intent (getApplicationContext(),HelpActivity.class));
			default:
			
				startActivity( new Intent(getApplicationContext(),ProfilPagerActivity.class));
				break;
			}
	}
}
