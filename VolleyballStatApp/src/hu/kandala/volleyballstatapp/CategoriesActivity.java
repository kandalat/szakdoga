package hu.kandala.volleyballstatapp;

import hu.kandala.adapters.CategoryListAdapater;
import hu.kandala.entities.Category;

import java.util.ArrayList;
import java.util.Arrays;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.widget.ListView;

public class CategoriesActivity extends ActionBarActivity

	{
		
		
		 ArrayList<Category> categories = new ArrayList<Category>(Arrays.asList(
	               new Category("Szerva", "Pontértékű", "elfogadható","Hiba",10,20,100),
	               new Category("Fogadás", "pontos","hazsnálható", "Hiba",30,30,10),
	               new Category("ütés", "Pontértékű","jó", "rossz",70,32,44),
	               new Category("Feladás","Pontos","Támadható","rossz",10,20,70),
	               new Category("Ejtés","pontértékű","pályára","rontott",30,60,50),
	               new Category("Ütésvédekezés","bejátszva","Támadható","elvesztet",10,90,30),
	               new Category("Ütés 2 es helyről","pointértékű","Jó","rontott",23,32,10)
	               )); 
	   

	@Override
	protected void onCreate(Bundle savedInstanceState)
		{

			
			
			super.onCreate(savedInstanceState);
			setContentView(R.layout.categories);
			
			ListView categories = (ListView) findViewById(R.id.categoriesListID);
			
			CategoryListAdapater adapter = new CategoryListAdapater(getApplicationContext(), R.layout.categories_list_item, this.categories);
			
			categories.setAdapter(adapter);
		}
	
	public boolean onCreateOptionsMenu(Menu menu)
		{

			return true;
		}
	}
