package hu.kandala.functions;

import java.util.ArrayList;
import java.util.Random;

import hu.kandala.entities.Category;

/**
 * 
 * @author KAndala Tamás
 *	A kategóriákhoz tartozó funkciókat tartalmazó osztály
 */
public class CategoryFunctions
	{

		/**
		 * Megnöveli eggyel az objektum jó és összértékét aztán újra számolja
		 * a százalékor a referencia százalékat és meghatározza újra az egy
		 * szavas kiértékelést
		 * 
		 * @return Visszaadja a megnövelt rossz értéket
		 */
		
		public Integer plusGoodValue(Category category)
			{
				category.goodValue++;
				category.Value++;
				category.setPercent();
				category.setReferencePercent();
				category.setEvalutaion();

				return category.goodValue;
			}

		/**
		 * Lecsökkenti eggyel az objektum jó és összértékét , ha a jó érték
		 * nagyobb mint 0 aztán újra számolja a százalékor a referencia
		 * százalékat és meghatározza újra az egy szavas kiértékelést
		 * 
		 * @return Visszaadja a csökkent jó értéket
		 */

		public Integer minusGoodValue(Category category)
			{
				if (category.goodValue <= 0) {
					return category.goodValue;
				}
				else {
					category.goodValue--;
					category.Value--;
				}
				category.setPercent();
				category.setReferencePercent();
				category.setEvalutaion();

				return category.goodValue;
			}

		/**
		 * Megnöveli eggyel az objektum rossz és összértékét aztán újra számolja
		 * a százalékor a referencia százalékat és meghatározza újra az egy
		 * szavas kiértékelést
		 * 
		 * @return Visszaadja a megnövelt rossz értéket
		 */

		public Integer plusBadValue(Category category)
			{
				category.badValue++;
				category.Value++;
				category.setPercent();
				category.setReferencePercent();
				category.setEvalutaion();

				return category.badValue;
			}

		/**
		 * Lecsökkenti eggyel az objektum rossz és összértékét , ha a rossz
		 * érték nagyobb mint 0 aztán újra számolja a százalékor a referencia
		 * százalékat és meghatározza újra az egy szavas kiértékelést
		 * 
		 * @return Visszaadja a csökkent rossz értéket
		 */

		public Integer minusBadValue(Category category)
			{
				if (category.badValue <= 0) {
					return category.badValue;
				}
				else {
					category.badValue--;
					category.Value--;
				}
				category.setPercent();
				category.setReferencePercent();
				category.setEvalutaion();
				return category.badValue;
			}

		/**
		 * Lecsökkenti eggyel az objektum közepes és összértékét , ha a közepes
		 * érték nagyobb mint 0 aztán újra számolja a százalékor a referencia
		 * százalékat és meghatározza újra az egy szavas kiértékelést
		 * 
		 * @return Visszaadja a csökkent közepes értéket
		 */

		public Integer minusNotBadValue(Category category)
			{
				if (category.notBadValue <= 0) {
					return category.notBadValue;
				}
				else {
					category.notBadValue--;
				}
				category.Value--;
				category.setPercent();
				category.setReferencePercent();
				category.setEvalutaion();

				return category.notBadValue;
			}

		/**
		 * Megnöveli eggyel az objektum közepes és összértékét aztán újra számolja
		 * a százalékor a referencia százalékat és meghatározza újra az egy
		 * szavas kiértékelést
		 * 
		 * @return Visszaadja a megnövelt rossz értéket
		 */
		
		public Integer plusNotBadValue(Category category)
			{
				category.notBadValue++;
				category.Value++;
				category.setPercent();
				category.setReferencePercent();
				category.setEvalutaion();
				
				return category.notBadValue;
			}
	

/**
 * A változatos megfogalmazás érdekében a szöveges kiértékeléshez
 * véletlenszerű kötő mondatokat ad az objektum listájában lévő catagory
 * objektumok randomText változójához
 * 
 * @return Visszaad egy listát amely már tartalmazza a
 *         szöveges kiértékeléshez szükséges véletlenszerűen generált
 *         töltelék mondatokkal
 */
public ArrayList<Category> randomizeEvalutate(ArrayList<Category> categories)
	{

		String[] texts = { "A vizsgált szempontok alapján",
				"A bevitt adatok szerint", "A mutatott játék szerint",
				"A statisztikai számítások eredményei szerint",
				"Az aktuális szettben nyújtott teljesítmény alapján",
				"A játék képének alakulásának következményében",
				"z eddig mutatott teljesítmény alapján"

		};
	
		Random rd = new Random();
		int lastindex = 0;
		int index = 0;

		for (Category r : categories) {
			while (lastindex == index) {
				index = rd.nextInt(texts.length);

			}

			r.setRandomText(texts[index]);

			lastindex = index;

		}
		return categories;
	}
}