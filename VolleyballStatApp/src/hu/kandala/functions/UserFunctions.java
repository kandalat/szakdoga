package hu.kandala.functions;

import hu.kandala.entities.User;

import java.util.ArrayList;

public class UserFunctions
	{
		User tamas = new User(1,"Tamás");
		private static ArrayList<User> userList;
		
		
		public UserFunctions()
			{
						userList= new ArrayList<User>();
					//	userList.add(tamas);
					//	userList.add(new User(2,"Zoli"));
						
			}
		
		public void createUser(int userID, String userName)
			{
				User result = new User(userID, userName);
				userList.add(result);
				
			}
		public ArrayList<User> getUserList()
		{
				return userList;				
		}
		public User searchUserByName(String name)
			{
				for (User u:userList)
				{
					if(u.getName().equals(name))
						return u;
					
				}
				return new User();
			}
		public User searchUserByID(int id)
			{
				for (User u:userList)
				{
					if(u.getUserID()==id)
						return u;
					
				}
				return new User();
			
			}
	
		public void deleteUserByID(int id)
			{
				for (User u: userList)
				{
					if (u.getUserID()==id)
					{
						userList.remove(u);						
					}
					
				}
				
			}
		
		
		
	}
