package hu.kandala.functions;

import hu.kandala.entities.Session;

import java.util.ArrayList;

public class SessionFunctions {

	private ArrayList<Session> sessionList;
	
	public SessionFunctions()
	{
		sessionList = new ArrayList<Session>();
		
		Session session1 = new Session(1, 1, "Csapat", "BME első szett");
		Session session2 = new Session(1,2,"Csapat","BME második szett");
		
		sessionList.add(session1);
		sessionList.add(session2);
	}
	
	public void addSessionToList(Session s)
	{
		sessionList.add(s);
	}
	
	public void deleteSessionByID(int id)
	{
		for (Session s : sessionList)
		{
			if ( s.getSessionID()==id)
				sessionList.remove(s);
		}
	}
	
	public Session searchSessionByID(int id)
	{
		for (Session s : sessionList)
		{
			if(s.getSessionID()==id)
			{
				return s;
			}
		}
		return new Session(0,0,"","");
		
	}
	public Session searchSessionByName(String name)
	{
		for (Session s : sessionList)
			{
			if( s.getName().equals(name))
			{
				return s;
			}
			}
		return new Session(0,0,"","");		
	}
	
	public ArrayList<Session> getSessionListByUser(int id)
	{
		ArrayList<Session> result = new ArrayList<Session>();
		for(Session s:sessionList)
		{
			if(s.getUserID()==id)
			{
				result.add(s);
			}	
		
		}
		if (!result.isEmpty())
			return result;
		else
			return new ArrayList<Session>();
		
	}
}
	