package hu.kandala.entities;

import java.util.ArrayList;

public class Session
	{
		private int userID, sessionID;
		private String type;
		private String name;
		private ArrayList<Category> categoryList;
		private Evaluation evaluation;
		private BarChart barChart;

		public Session(int userID, int sessionID, String type,
				ArrayList<Category> categoryList, Evaluation evaluation,
				BarChart barChart)
			{
				super();
				this.userID = userID;
				this.sessionID = sessionID;
				this.type = type;
				this.categoryList = categoryList;
				this.evaluation = evaluation;
				this.barChart = barChart;
			}

		public Session(int userID, int sessionID, String type, String name)
			{
				super();
				this.userID = userID;
				this.sessionID = sessionID;
				this.type = type;
				this.name = name;
			}

		public int getUserID()
			{
				return userID;
			}

		public void setUserID(int userID)
			{
				this.userID = userID;
			}

		public int getSessionID()
			{
				return sessionID;
			}

		public void setSessionID(int sessionID)
			{
				this.sessionID = sessionID;
			}

		public String getType()
			{
				return type;
			}

		public void setType(String type)
			{
				this.type = type;
			}

		public ArrayList<Category> getCategoryList()
			{
				return categoryList;
			}

		public void setCategoryList(ArrayList<Category> categoryList)
			{
				this.categoryList = categoryList;
			}

		public Evaluation getEvaluation()
			{
				return evaluation;
			}

		public void setEvaluation(Evaluation evaluation)
			{
				this.evaluation = evaluation;
			}

		public BarChart getBarChart()
			{
				return barChart;
			}

		public void setBarChart(BarChart barChart)
			{
				this.barChart = barChart;
			}

		public String getName()
			{
				return name;
			}

		public void setName(String name)
			{
				this.name = name;
			}

	}
