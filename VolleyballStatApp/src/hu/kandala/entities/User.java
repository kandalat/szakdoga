package hu.kandala.entities;

import java.util.ArrayList;

public class User
	{
		private int userID;
		private String userName;
		private ArrayList<Session> userSessionList;
		private String name, birthDate, city, team, championship;
		private String position, position2;

		public User(int userID, String userName)
			{
				super();
				this.userID = userID;
				this.userName = userName;
			
				this.userSessionList = new ArrayList<Session>();
				this.name = "";
				this.birthDate = "";
				this.city = "";
				this.team = "";
				this.championship = "";
				this.position = "";
				this.position2 = "";
			}

		public User()
			{
				// TODO Auto-generated constructor stub
			}

		public int getUserID()
			{
				return userID;
			}

		public void setUserID(int userID)
			{
				this.userID = userID;
			}

		public String getUserName()
			{
				return userName;
			}

		public void setUserName(String userName)
			{
				this.userName = userName;
			}

	

		public ArrayList<Session> getUserSessionList()
			{
				return userSessionList;
			}

		public void setUserSessionList(ArrayList<Session> userSessionList)
			{
				this.userSessionList = userSessionList;
			}

		public String getName()
			{
				return name;
			}

		public void setName(String name)
			{
				this.name = name;
			}

		public String getBirthDate()
			{
				return birthDate;
			}

		public void setBirthDate(String birthDate)
			{
				this.birthDate = birthDate;
			}

		public String getCity()
			{
				return city;
			}

		public void setCity(String city)
			{
				this.city = city;
			}

		public String getTeam()
			{
				return team;
			}

		public void setTeam(String team)
			{
				this.team = team;
			}

		public String getChampionship()
			{
				return championship;
			}

		public void setChampionship(String championship)
			{
				this.championship = championship;
			}

		public String getPosition()
			{
				return position;
			}

		public void setPosition(String position)
			{
				this.position = position;
			}

		public String getPosition2()
			{
				return position2;
			}

		public void setPosition2(String position2)
			{
				this.position2 = position2;
			}

		
	}
