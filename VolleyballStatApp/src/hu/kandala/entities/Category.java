package hu.kandala.entities;



/**
 *
 * @author Kandala Tamás
 * 
 * A Category osztály a program alapvető viselkedését modellezi.
 * Az osztály egyes példányai egy-egy vizsgált kategóriát reprezentálnak.
 * 
 */
public class Category {
    /** 
     *  A Category osztály változói:
     * Stringek : Kategória neve, jó érték neve, közepes érték neve, 
     * rossz érték neve, kiértékelés, randomszöveg
     * Integerek: Összesített érték,jó érték, közepes érték, 
     * rossz érték, százalék érték, referencia százalék
     */
   public String Name, goodValueName, notBadValueName, badValueName, evalutaion,
            randomText;
    public Integer Value, goodValue, notBadValue, badValue, percent, 
            referencePercent;

    /**
     * Üres Category konstruktor
     */

    public Category() {
    }
    
    /** Category konstruktor 
     * 
     * Értéket ad az osztály változóinak a megadott paraméterek alapján,
     * a value változó a goodValue a notBadValue és a badValue összege,
     * a percent , a referencePercent és a evaluation értékek a set metódusok
     * segítségével kapnak értéket mert számított értékek. 
     * Fájl olvasásnál optimális.
     * 
     * @param Name a kategória neve 
     * @param goodValueName jóérték neve
     * @param notBadValueName közepesérték neve
     * @param badValueName rosszérték neve
     * @param goodValue jóérték
     * @param notBadValue közepesérték
     * @param badValue rosszérték
     */
    public Category(String Name, String goodValueName, String notBadValueName, String badValueName, Integer goodValue, Integer notBadValue, Integer badValue) {
        this.Name = Name;
        this.goodValueName = goodValueName;
        this.notBadValueName = notBadValueName;
        this.notBadValue = notBadValue;
        this.badValueName = badValueName;
        this.goodValue = goodValue;
        this.badValue = badValue;
        this.Value = badValue + goodValue + notBadValue;
        this.setPercent();
        this.setReferencePercent();
        this.setEvalutaion();


    }

    /**
     * 
     * Értéket ad az osztály változóinak példányosításkor csak a String típusú 
     * változókat határozzuk meg az Integer tipusú váltzoók 0 értéket kapnak.
     * a value változó a goodValue a notBadValue és a badValue összege,
     * a percent , a referencePercent és a evaluation értékek a set metódusok
     * segítségével kapnak értéket mert számított értékek. 
     * 
     * Új 0 értékkel rendelkező objektumot hoz létre
     * 
     * @param Name Kategória neve
     * @param goodValueName jóérték neve
     * @param notBadValueName közepesérték neve
     * @param badValueName  rosszérték neve
     */
    
    public Category(String Name, String goodValueName, String notBadValueName, String badValueName) {
        this.Name = Name;
        this.goodValueName = goodValueName;
        this.notBadValueName = notBadValueName;
        this.badValueName = badValueName;
        this.goodValue = 0;
        this.badValue = 0;
        this.notBadValue = 0;
        this.Value = badValue + goodValue + notBadValue;
        this.setPercent();
        this.setReferencePercent();
        this.setEvalutaion();


    }

    /**
     * 
     * @return Vissza adja az objektum Name változóját 
     */
    
    public String getName() {
        return Name;
    }
    
    /**
     * 
     * @param Name beállítja a Name változó tartalmát
     */
    
    public void setName(String Name) {
        this.Name = Name;
    }

    /**
     * 
     * @return Visszaadja az objektum jó értékéhez tartozó megnevezést
     */
    public String getGoodValueName() {
        return goodValueName;
    }

    /**
     * 
     * @param goodValueName Beállítja az objektum jó értékhet tartozó megnevzést 
     */
    
    public void setGoodValueName(String goodValueName) {
        this.goodValueName = goodValueName;
    }

    /**
     * 
     * @return Visszaadja az objektum közepes értékéhez tartozó megnevezést 
     */
    
    public String getNotBadValueName() {
        return notBadValueName;
    }
    /**
     * 
     * @param notBadValueName Beállítja az objektum jó értékhet tartozó megnevzést 
     */
    
    public void setNotBadValueName(String notBadValueName) {
        this.notBadValueName = notBadValueName;
    }
    
    /**
     * 
     * @return Visszaadja az objektum rossz értékéhez tartozó megnevezést  
     */
    
    public String getBadValueName() {
        return badValueName;
    }

    /**
     * 
     * @param badValueName Beállítja az objektum jó értékhet tartozó megnevzést  
     */
    
    public void setBadValueName(String badValueName) {
        this.badValueName = badValueName;
    }

    /**
     * 
     * @return Visszaadja az objektumhoz tartozó összértéket 
     */
    
    public Integer getValue() {
        return Value;
    }

    /**
     * 
     * @return Visszaadja az objektumhoz tartozó jó értéket 
     */
    
    public Integer getGoodValue() {
        return goodValue;
    }

    /**
     * 
     * @param goodValue Beállítja az objektum jó értékét
     */
    
    public void setGoodValue(Integer goodValue) {
        this.goodValue = goodValue;
    }

    /**
     * 
     * @return Visszaadja az objektumhoz tartozó közepes értéket 
     */
    
    public Integer getNotBadValue() {
        return notBadValue;
    }

    /**
     * 
     * @param notBadValue Beállítja az objektum közepes értékét
     */
    
    public void setNotBadValue(Integer notBadValue) {
        this.notBadValue = notBadValue;
    }

    /**
     * 
     * @return Visszaadja az objektum rossz értékét 
     */
    
    public Integer getBadValue() {
        return badValue;
    }

    /**
     * 
     * @param badValue beállítja az objektumhoz tartozó rossz értéket
     */
        
    public void setBadValue(Integer badValue) {
        this.badValue = badValue;
    }

    /**
     * A setPercent metódus beállítja az objektum százalék
     * Ez egy százalék számítás ami meghatározza egy modellezett kategória 
     * hiba-százalékát.
     * Ha az objekt Value vagy goodValue és notBadValue értéke 0 
     * akkor a percent értéke 0 lesz.
     * 
     */
    
    public void setPercent() {
        if (this.Value.equals(0) || (this.goodValue.equals(0)& this.notBadValue.equals(0))) {
            this.percent = 0;
        } else {
            this.percent = (this.goodValue +this.notBadValue)* 100 / this.Value;
        }
    }
    
/**
 * 
 * @return Visszaadja az objektum százalék változóját
 */
    
    public Integer getPercent() {
        return percent;
    }

 /**
     * Beállítja az objektumhoz tartozó referencia százalék értéket,
     * ami a kategória kiértékeléséhez szükséges.
     * 
     */
    
    public void setReferencePercent() {
        if (this.percent < 26) {
            this.referencePercent = 25;
        } else if (this.percent > 25 && this.percent < 51) {
            this.referencePercent = 50;
        } else if (this.percent > 50 && this.percent < 76) {
            this.referencePercent = 75;
        } else if (this.percent > 75 && this.percent < 101) {
            this.referencePercent = 100;
        }
    }

    /**
     * 
     * @return Visszaadja az objektumhoz tartozó referencia százalélkot
     */
    
    public Integer getReferencePercent() {
        return referencePercent;
    }

    /**
     * 
     * @return Visszaadja az objektumhoz tartozó egy szavas értékelést  
     */
    
    public String getEvalutaion() {
        return evalutaion;
    }

    
    /**
     * A referencia százalék alapján meghatározza
     * az objektumhoz tartozó kiértékelést
     */
    public void setEvalutaion() {
        if (this.referencePercent.equals(25)) {
            this.evalutaion = "elfogadhatatlan";
        } else if (this.referencePercent.equals(50)) {
            this.evalutaion = "gyenge";
        } else if (this.referencePercent.equals(75)) {
            this.evalutaion = "átlagos";
        } else if (this.referencePercent.equals(100)) {
            this.evalutaion = "kiemelkedő";
        }
    }

    /**
     * 
     * @return Visszaadja az objektumhoz tartozó véletlenszerűen generált 
     * szöveget amely a szöveges kiértékeléshez szükséges
     */
    
    public String getRandomText() {
        return randomText;
    }

    /**
     * 
     * @param randomText Beállítja a szöveges értékeléshez szökséges
     * véletlenszerűen generált szöveget
     */
    public void setRandomText(String randomText) {
        this.randomText = randomText;
    }
        
  
/**
 * 
 * 
 * @return Visszaadja az objektumhoz tartozó szöveges kirétékelést
 */
    @Override
    public String toString() {
        return this.getName()+":"+" "+this.getRandomText()+" "+this.getEvalutaion()+".";
               
    }
    /**
     *  Kiírja a parncssorra az objektum toString metódusának visszatérési értékét
     */
    
    public void print() {
        System.out.print(this.toString());
    }
}
   